use "~/.config/nushell/modules/prompt.nu"

$env.PATH = ($env.PATH | split row (char esep)
  | append $"($env.HOME)/.local/bin"
  | append $"($env.HOME)/.cargo/bin")

$env.PROMPT_COMMAND = { || prompt left_prompt  }
$env.PROMPT_COMMAND_RIGHT = { || prompt right_prompt }
$env.PROMPT_INDICATOR = " "

$env.EDITOR = "helix"
$env.GTK_IM_MODULE = "fcitx"        
$env.QT_IM_MODULE = "fcitx"         
$env.XMODIFIERS = "@im=fcitx"       
$env.QT_QPA_PLATFORM = "wayland"
$env.FZF_DEFAULT_OPTS = '
  --color=fg:-1,fg+:#d0d0d0,bg:-1,bg+:#262626
  --color=hl:#5f87af,hl+:#5fd7ff,info:#afaf87,marker:#87ff00
  --color=prompt:#d7005f,spinner:#af5fff,pointer:#af5fff,header:#87afaf
  --color=border:#262626,label:#aeaeae,query:#d9d9d9
  --border="rounded" --border-label="Server List" --preview-window="border-rounded" --prompt="> "
  --marker=">" --pointer="◆" --separator="─" --scrollbar="│"
'

