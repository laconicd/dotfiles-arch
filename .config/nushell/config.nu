# functions
def GPU [] { 
  git add .
  git commit -m (date now | format date '%Y.%m.%d')
  git push origin main 
}

def S [] {
  let SVR = (rg ^Host ~/.ssh
    | detect columns -n
    | get column1
    | filter {|el| $el != '*'}
    | to text
    | fzf)
  if not ($SVR | is-empty) {
    ssh $SVR
  } 
}

# Aliases
alias l = ls
alias ll = ls -a
alias lll = ls -al
alias x = helix
alias z = zoxide
alias Z = zellij
alias b = broot                        
alias pacs = sudo pacman -Ss
alias pacu = sudo pacman -Syu
alias paci = sudo pacman -S
alias pacr = sudo pacman -Rsn
alias pacc = sudo pacman -Scc
alias pacro = sudo bash -c "pacman -Qtdq | pacman -Rns -"

# Starship
use ~/.cache/starship/init.nu
