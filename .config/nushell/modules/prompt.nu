# LEFT PROMPT
export def left_prompt [] {
  let term_bg = '#161616'

  let shell_bg = '#d26939'
  let shell_fg = '#d3ebe9'

  let user_bg = '#26a98b'
  let user_fg = '#d3ebe9'

  let path_bg = '#33859d'
  let path_fg = '#d3ebe9'

  let git_bg = '#599caa'
  let git_fg = '#d3ebe9'

  let exit_code_symbol = if $env.LAST_EXIT_CODE == 0 { "   " } else { "   " }
  let exit_code_bg = if $env.LAST_EXIT_CODE == 0 { '#245361' } else { '#c33027' }
  let exit_code_fg = '#98d1ce'

  let cmd_line_bg = '#edb54b'
  
  [
    (tailing_diamond $term_bg $shell_bg '')
    (shell_segment $shell_bg $shell_fg)
    (leading_diamond $term_bg $shell_bg '')

    (tailing_diamond $user_bg $term_bg '')
    (user_segment $user_bg $user_fg)
    (leading_diamond $term_bg $user_bg '')

    (tailing_diamond $path_bg $term_bg '')
    (path_segment $path_bg $path_fg)
    (leading_diamond $git_bg $path_bg '')

    (git_segment $git_bg $git_fg)
    (leading_diamond $term_bg $git_bg '')

    (tailing_diamond $exit_code_bg $term_bg '')
    (last_exit_code_segment $exit_code_bg $exit_code_fg $exit_code_symbol)
    (leading_diamond $term_bg $exit_code_bg '')

    (cmd_line $term_bg $cmd_line_bg)
  ] | str join
}  

def set_color [bg fg text attr = n] {  
  [(ansi { bg: $bg fg: $fg attr: $attr }) $text] | str join
}

def get_osname [] {
  ($nu.os-info | get name)
}

def leading_diamond [bg = black fg = white diamond = ''] {
  (set_color $bg $fg $diamond)
}

def tailing_diamond [bg = black fg = white diamond = ''] {
  (set_color $bg $fg $diamond)
}

def separating_diamond [bg = black fg = white diamond = ''] {
  (set_color $bg $fg $diamond)
}

def shell_segment [bg = black fg = white] {
  (set_color $bg $fg '   ')
}

def user_segment [bg = black fg = white] {
  (set_color $bg $fg $" ($env.USER) ")
}

def path_segment [bg = black fg = white] {
  let is_home = ($env.PWD == $env.HOME) 
  let path = if $is_home { '~' } else { ($env.PWD | path basename) }
  (set_color $bg $fg $"   ($path) ")        
}

def git_segment [bg = black fg = white ] {
  let is_git = (".git" | path exists)
  if (".git" | path exists) {
    (set_color $bg $fg ($"   (git branch | str trim -c '*' | str trim) "))
  }
}

def last_exit_code_segment [bg = black fg = white symbol = ''] { 
  (set_color $bg $fg $"($symbol)" )
}

def cmd_line [bg = black fg = white] {
  (set_color $bg $fg $"\n╰─󰘁")
}

# RIGHT PROMPT
export def right_prompt [] {
  let term_bg = '#161616'

  let cmd_duration_bg = '#c2454e'
  let cmd_duration_fg = '#e8dfd6'

  let distro_symbol_bg = '#7cbf9e'
  let distro_symbol_fg = '#2e3340'

  let now_bg = '#44b5b1'
  let now_fg = '#2e3340'

  [
    (leading_diamond $term_bg $cmd_duration_bg '')
    (cmd_duration $cmd_duration_bg $cmd_duration_fg)
    (tailing_diamond $cmd_duration_bg $term_bg '')

    (leading_diamond $term_bg $distro_symbol_bg '')
    (distro_symbol $distro_symbol_bg $distro_symbol_fg)
    (tailing_diamond $distro_symbol_bg $term_bg '')

    (leading_diamond $term_bg $now_bg '')
    (now $now_bg $now_fg)
    (tailing_diamond $term_bg $now_bg '')
  ] | str join
}  

def cmd_duration [bg = black fg = white] {
  let duration = if $env.CMD_DURATION_MS == '0823' { 0 } else { $env.CMD_DURATION_MS } 
  (set_color $bg $fg $" 󰔟 ($duration) ms ")
}

def distro_symbol [bg = black fg = white] {
  (set_color $bg $fg $'   ')
}

def now [bg = black fg = white] { 
  (set_color $bg $fg $" 󱛡  (date now | format date '%d,%H:%M') ")
}
