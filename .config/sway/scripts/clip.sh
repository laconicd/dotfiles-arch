#!/usr/bin/env bash

FUZZEL="fuzzel --dmenu"
cliphist list | ${FUZZEL} | cliphist decode | wl-copy
