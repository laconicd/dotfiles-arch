#!/usr/bin/env bash

FORMAT="[W:%W]  %A - %T"
FUZZEL="fuzzel --dmenu"

# FORMAT as a `jq` concatenation string
FORMAT="$FORMAT (%I)"
FORMAT=$(echo "$FORMAT" | \
        sed  's/%O/" + .output + "/
              s/%W/" + .workspace + "/
              s/%A/" + .app_id + "/
              s/%T/" + .name + "/
              s/%I/" + .id + "/
              s/"/\"/
              s/\(.*\)/\"\1\"/')

# Get the container ID from the node tree
CON_ID=$(swaymsg -t get_tree | \
    jq -r ".nodes[]
        | {output: .name, content: .nodes[]}
        | {output: .output, workspace: .content.name,
          apps: .content
            | ..
            | {id: .id?|tostring, name: .name?, app_id: .app_id?, shell: .shell?}
            | select(.app_id != null or .shell != null)}
        | {output: .output, workspace: .workspace,
           id: .apps.id, app_id: .apps.app_id, name: .apps.name }
        | $FORMAT
	| tostring" | $FUZZEL)

# Requires the actual `id` to be at the end and between paretheses
CON_ID=${CON_ID##*(}
CON_ID=${CON_ID%)}

# Focus on the chosen window
swaymsg [con_id=$CON_ID] focus
