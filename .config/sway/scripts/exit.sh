#!/usr/bin/env bash

swaynag \
  --text=ffffff \
  --background=2e3440ff \
  -f "VictorMono Nerd Font Mono italic 15" \
  -t warning -m 'Quit' \
  -b 'Yes, exit sway' 'swaymsg exit'
