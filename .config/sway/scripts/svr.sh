#!/usr/bin/env bash

FUZZEL="fuzzel --dmenu"
host=$(rg ^Host ~/.ssh | rg -v "\\*|gitlab-me" | cut -f 2 -d " " | $FUZZEL)
alacritty -e ssh $host 
