#!/usr/bin/env bash

# update packages
sudo pacman -Syu

# install packages
# Intel
# vulkan-intel intel-media-driver libva-intel-driver libva-utils
# AMD
# amd-ucode amdvlk
sudo pacman -Sy mesa libva-mesa-driver mesa-vdpau vulkan-virtio \
  bluez bluez-utils rtkit networkmanager-l2tp \
  git base-devel sway swaybg swayidle swaylock swayimg \
  firefox alacritty fuzzel fnott cliphist nnn jq fzf \
  nushell starship helix ripgrep fd gitui \
  ttf-victor-mono-nerd noto-fonts noto-fonts-cjk \
  fcitx5 fcitx5-hangul fcitx5-configtool fcitx5-gtk fcitx5-qt qt5-wayland

# install yay
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin
makepkg -si
cd ..
rm -rf yay-bin

# seatd service
sudo usermod -aG seat me
sudo systemctl enable --now seatd

# copy configs
cp -r .config ~/
cp -r .zoxide ~/
cp -r Wallpapers ~/

# ansible
sudo pacman -S python-pipx sshpass
pipx install --include-deps ansible jmespath pynetbox pywinrm netaddr

# rust
sudo pacman -S rustup rust-analyzer
rustup default stable

# misc
yay -S visual-studio-code-bin google-chrome

# starship
mkdir ~/.cache/starship
starship init nu | save -f ~/.cache/starship/init.nu
