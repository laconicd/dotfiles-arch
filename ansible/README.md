# Installing Packages

## Fedora/RHEL 
```sh
dnf install sshpass python3-pip
```

## Ansible
```sh
pip install --user ansible jmespath pynetbox netaddr dnspython
```

## Remote Machine
###### _(**lshw >= B.02.19.2** 필요)_


